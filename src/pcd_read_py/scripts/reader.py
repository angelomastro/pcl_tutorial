#! /usr/bin/python2.7

import pcl

from sys import argv
from sys import exit

class PcdReader:
    def __init__(self, file_path):
        print "opening pcd file: %s" % (file_path)
        self.file_path = file_path
        # ascii files
        self.cloud = pcl.PointCloud()
        self.cloud.from_file(file_path)
        # binary files
        #self.cloud = pcl.load(file_path) 

    def print_points(self):
        print "imported %i points" % (self.cloud.size)
        print "first 10 points"
        for i in range (0, 10):
            print "point %i: %s" % (i, self.cloud[i])

    def size(self):
        return self.cloud.size

def read_file(file_path):
    reader = PcdReader(file_path)
    reader.print_points()


if __name__ == "__main__":
    
    if len(argv) <= 1:
        print "$ reader.py <pcd_file>"
        print "PCD file reader in python"
        exit(0)
        
    read_file(argv[1])
