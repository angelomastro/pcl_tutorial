from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup
d = generate_distutils_setup(
    packages=['pcd_read_py'],
    scripts=['scripts/reader.py'],
    package_dir={'': 'src'}
)
setup(**d)