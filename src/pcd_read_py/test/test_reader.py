import unittest

import os, sys
rel_path = os.path.dirname(os.path.realpath(__file__)) + "/../scripts"
sys.path.append(rel_path)

from reader import PcdReader

class CaseAsciiFile(unittest.TestCase):
    
    def test_print(self):
        data = "/home/angelo/development/pcl_tutorial/data/read.ply" #"../data/read.ply"
        r = PcdReader(data)
        r.print_points()
        self.assertEqual(data, r.file_path)
        self.assertEqual(213, r.size())
        p1 = r.cloud[0] # tuple (x,y,z) of the first point of the cloud
        self.assertEqual(0.94, round(p1[0], 2))
        self.assertEqual(0.34, round(p1[1], 2))
        self.assertEqual(0.00, round(p1[2], 2))


if __name__ == '__main__':
    unittest.main()