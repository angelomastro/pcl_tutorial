#include <iostream>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
//#include <pcl/point_cloud.h>

int main (int argc, char** argv)
{
    if (argc <= 1)
    {
        std::cerr << "ply_reader <pcd_file_path>" << std::endl;
        return (1);
    }

    std::string filePath(argv[1]);

//    pcl::PLYReader reader;

//    plc::PointCLoud rCloud;
//    Eigen::Vector4f rOrigin;
//    Eigen::Quaternionf rOrientation;
//    int rPlyVersion;

//    reader.read(filePath, rCloud, rOrigin, rOrientation, rPlyVersion);

//    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

//    if (pcl::io::loadPCDFile<pcl::PointXYZ> (filePath, *cloud) == -1) //* load the file
//    {
//        PCL_ERROR ("Couldn't read file test_pcd.pcd \n");
//        return (-1);
//    }
//    std::cout << "Loaded "
//            << cloud->width * cloud->height
//            << " data points from test_pcd.pcd with the following fields: "
//            << std::endl;
//    for (size_t i = 0; i < cloud->points.size (); ++i)
//        std::cout << "    " << cloud->points[i].x
//                  << " "    << cloud->points[i].y
//                  << " "    << cloud->points[i].z << std::endl;

    return (0);
}
