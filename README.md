## Point Cloud Tutorial

I will use the [pcl library](http://docs.pointclouds.org/trunk/) to make some test in c++ and python. 

* reading / writing a cloud of points 

* transforming it into a CAD shape


#### Requirements

* It is based on [ROS](http://wiki.ros.org/ROS/Tutorials) and uses **catkin** to build. I use version *Kinetic* but any one would do

* VTK is a PCL requirement. Download and install the [tarball](https://www.vtk.org/files/release/8.1/VTK-8.1.1.tar.gz)

* PCL library. [Git clone](https://github.com/PointCloudLibrary/pcl), build and install it

* If you too are using Qt IDE, you want to read how to [setup QtCreator](http://wiki.ros.org/IDEs#QtCreator) to make it navigate and compile a catkin project. 

* For **python** the followings need to be installed (assuming ubuntu 16.04). 

```
pip install numpy
sudo easy_install cython # pip install cython

Download https://github.com/strawlab/python-pcl/archive/v0.2.0.zip

tar zxf python-pcl-x.x.x.tar.gz
cd python-pcl-x.x.x
python setup.py install

```

After that you can run `import pcl` and use the python [bindinds](https://media.readthedocs.org/pdf/python-pcl-fork/latest/python-pcl-fork.pdf)


#### Build 

```
catkin_make
```

#### Run

```
./devel/lib/pcd_read/pcd_reader data/read.pcd
./devel/lib/pcd_write/pcd_writer data/written.ply
./devel/lib/pcd_write/rotate data/cube.ply
```

#### Tests

##### Python (simple unit test)

Run them with catkin ... 

```
catkin_make test 
```

or 

```
catkin_make run_tests
```

... or  individually (debug)

```
python src/pcd_read_py/test/test_reader.py
```

the result will be (for example) 

```
opening pcd file: /home/angelo/development/pcl_tutorial/data/read.ply
imported 213 points
first 10 points
point 0: (0.9377300143241882, 0.3376300036907196, 0.0)
point 1: (0.908050000667572, 0.3564099967479706, 0.0)
point 2: (0.8191499710083008, 0.3199999928474426, 0.0)
point 3: (0.9719200134277344, 0.27799999713897705, 0.0)
point 4: (0.9440000057220459, 0.2947399914264679, 0.0)
point 5: (0.9811099767684937, 0.24246999621391296, 0.0)
point 6: (0.9365500211715698, 0.2614299952983856, 0.0)
point 7: (0.9163100123405457, 0.27441999316215515, 0.0)
point 8: (0.8192099928855896, 0.29315000772476196, 0.0)
point 9: (0.907010018825531, 0.24108999967575073, 0.0)
.
----------------------------------------------------------------------
Ran 1 test in 0.001s

OK

```
